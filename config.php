<?php /*
 * Copyright (C) 2017 All rights reserved.
 *   
 * @File config.php
 * @Brief 
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-27
 *
 */
 
return array(
    'DEBUG' => true, // 是否打印debug信息
    'redie_host' => '127.0.0.1', //redis主机地址
    'redis_port' => 6379, //redis端口号
    'redis_db' => 2, //redis 数据库
    'redis_prefix' => 'wx_', //redis 前缀

);
